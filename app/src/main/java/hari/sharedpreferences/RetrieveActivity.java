package hari.sharedpreferences;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class RetrieveActivity extends AppCompatActivity {
    private TextView retrieved_text;
    private Button retrieve_data;
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_retrive);
        //pull views
        retrieved_text = (TextView) findViewById(R.id.retrieved_text);
        retrieve_data = (Button) findViewById(R.id.retrieve_data);

        //get SP
        sharedPreferences = getSharedPreferences("MyData", MODE_PRIVATE);

        retrieve_data.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                retrieved_text.setText(sharedPreferences.getString("data", "no data with this key"));
            }
        });

    }
}
